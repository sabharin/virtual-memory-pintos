# README #

Virtual Memory Implementation for Pintos Operating System, a simple operating system framework for the 80x86 architecture. 
LRU Second Chance Algorithm is implemented for Frame Eviction during Page Fault

Take a look at the Design document and the desicions made at https://bitbucket.org/sabharin/virtual-memory-pintos/src/b03abe5ba873e96f461e78289403dabb43b263b2/DESIGNDOC?at=master