#include "vm/page.h"
#include <stdio.h>
#include <string.h>
#include "vm/frame.h"
#include "vm/swap.h"
#include "filesys/file.h"
#include "threads/malloc.h"
#include "threads/thread.h"
#include "userprog/pagedir.h"
#include "threads/vaddr.h"

/* Maximum size of process stack, in bytes. */

bool pagetable_create(struct hash *pagetable){

//	printf("Initializing a page table \n");
	hash_init (pagetable, page_hash, page_less, NULL);
	lock_init (&(thread_current()->pagetable_lock));
	if(!pagetable){
		return false;
	}
	return true;
}

/* Destroys a page, which must be in the current process's
   page table.  Used as a callback for hash_destroy(). */

static void
destroy_page (struct hash_elem *p_, void *aux UNUSED)
{
//	printf("Destroy a page table entry in page table structure \n");
	struct page* desPage = hash_entry(p_,struct page,hashElem);
	// Clear the frame / swap contents if it is loaded in memory
//	if(desPage->frame!=NULL){
//		free(desPage->frame);
//	}
	free(desPage);
//	printf("Cleared a page table entry \n");
}

/* Destroys the current process's page table. */
void
page_exit (struct hash *pagetable) 
{
//	printf("Destroying current page table \n");
	lock_acquire(&(thread_current()->pagetable_lock));
	hash_destroy(pagetable,destroy_page);
	lock_release(&(thread_current()->pagetable_lock));
//	printf("Destroy of page table complete \n");
}

/* Returns the page containing the given virtual ADDRESS,
   or a null pointer if no such page exists.
   Allocates stack pages as necessary. */
struct page *
page_for_addr (const void *address) 
{
	lock_acquire(&(thread_current()->pagetable_lock));	
	struct hash pagetable = thread_current()->supPageTable;
	struct page p;
	struct hash_elem *e;
	p.addr = pg_round_down(address);
	e = hash_find (&pagetable, &p.hashElem);
	//		printf("Found a page entry \n");
	lock_release(&(thread_current()->pagetable_lock));
	return e != NULL ? hash_entry (e, struct page, hashElem) : NULL;
	return NULL;
}

/* Locks a frame for page P and pages it in.
   Returns true if successful, false on failure. */
bool
do_page_in (struct page *p)
{
	lock_acquire(&(p->page_lock));
	if(!(p->swapped)){
		p->frame = frame_alloc_and_lock(p);
//		printf("Frame alloc\n");
		if(p->frame != NULL){
			p->loaded = true;
		//	printf("Allocated a page \n");
			return true;

		}
	}else{
//		printf("Allocating a page from swap \n");
		p->frame = frame_alloc_swap(p);
		return true;
	}
	lock_release(&(p->frame->frame_lock));
	lock_release(&(p->page_lock));
//	printf("FAILED to allocate a page \n");
	return false;
}

/* Faults in the page containing FAULT_ADDR.
   Returns true if successful, false on failure. */
bool
page_in (void *fault_addr) 
{
	struct hash pagetable = thread_current()->supPageTable;
	struct page* newPage = (struct page *)malloc(sizeof(struct page));	
	if(newPage == NULL){
		return false;
	}
//	newPage->file = file;
//	newPage->ofs = ofs;
//	newPage->readBytes = page_read_bytes;
	newPage->addr = pg_round_down(fault_addr);
	newPage->writable = true;
	newPage->loaded = false;
	newPage->swapped = false;
	newPage->pagedir = thread_current()->pagedir;
//	printf("Added a page table offset %jd\n",ofs);
//	printf("Added a page table entry read bytes %d \n",page_read_bytes);
//	printf("Added a page table entry addr %p\n",vaddr);
	lock_acquire(&(thread_current()->pagetable_lock));
	hash_insert(&pagetable, &newPage->hashElem);	
	lock_release(&(thread_current()->pagetable_lock));
	newPage->frame = frame_alloc_stack(newPage);
	lock_init(&(newPage->page_lock));
	if(newPage->frame != NULL){
		newPage->loaded = true;
	}
	lock_release(&(newPage->frame->frame_lock));
	return newPage->loaded;
}

/* Evicts page P.
   P must have a locked frame.
   Return true if successful, false on failure. */
bool
page_out (struct page *p) 
{
	frame_unlock(p->frame);
	frame_free(p->frame);
	p->loaded = false;
}

/* Returns true if page P's data has been accessed recently,
   false otherwise.
   P must have a frame locked into memory. */
bool
page_accessed_recently (struct page *p) 
{
}

/* Adds a mapping for user virtual address VADDR to the page hash
   table.  Fails if VADDR is already mapped or if memory
   allocation fails. */
struct page *
page_allocate (struct file *file, off_t ofs,size_t page_read_bytes,void *vaddr, bool writable)
{
	//Creating a new page for lazy loading
	struct page* newPage = (struct page *)malloc(sizeof(struct page));	
	if(newPage == NULL){
		return newPage;
	}
	newPage->file = file;
	newPage->ofs = ofs;
	newPage->readBytes = page_read_bytes;
	newPage->addr = vaddr;
	newPage->writable = writable;
	newPage->loaded = false;
	newPage->swapped = false;
	newPage->pagedir = thread_current()->pagedir;
	lock_init(&(newPage->page_lock));
//	printf("Added a page table offset %jd\n",ofs);
//	printf("Added a page table entry read bytes %d \n",page_read_bytes);
//	printf("Added a page table entry addr %p\n",vaddr);
	return newPage;
}

/* Evicts the page containing address VADDR
   and removes it from the page table. */
void
page_deallocate (void *vaddr) 
{
	struct page *page = page_for_addr(vaddr);
	if(page != NULL){
		destroy_page(&(page->hashElem), NULL);
	}
}

/* Returns a hash value for the page that E refers to. */
unsigned
page_hash (const struct hash_elem *e, void *aux UNUSED) 
{
	const struct page *p = hash_entry (e, struct page, hashElem);
	return hash_bytes (&p->addr, sizeof p->addr);
}

/* Returns true if page A precedes page B. */
bool
page_less (const struct hash_elem *a_, const struct hash_elem *b_,
           void *aux UNUSED) 
{
	const struct page *a = hash_entry (a_, struct page, hashElem);
	const struct page *b = hash_entry (b_, struct page, hashElem);
	return a->addr < b->addr;
}

/* Tries to lock the page containing ADDR into physical memory.
   If WILL_WRITE is true, the page must be writeable;
   otherwise it may be read-only.
   Returns true if successful, false on failure. */
bool
page_lock (const void *addr, bool will_write) 
{
	struct page* page = page_for_addr(addr);
	lock_acquire(&(page->page_lock));
}

/* Unlocks a page locked with page_lock(). */
void
page_unlock (const void *addr) 
{
	struct page* page = page_for_addr(addr);
	lock_release(&(page->page_lock));
}

bool page_eviction(){
//	struct frame *frame = evict_frame();	
}
