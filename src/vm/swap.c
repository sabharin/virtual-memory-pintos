#include "vm/swap.h"
#include <bitmap.h>
#include <debug.h>
#include <stdio.h>
#include "vm/frame.h"
#include "vm/page.h"
#include "threads/synch.h"
#include "threads/vaddr.h"
#include "devices/block.h"

/* The swap device. */
static struct block *swap_device;

/* Used swap pages. */
static struct bitmap *swap_bitmap;

/* Protects swap_bitmap. */
static struct lock swap_lock;

/* Number of sectors per page. */
#define PAGE_SECTORS (PGSIZE / BLOCK_SECTOR_SIZE)

/* Sets up swap. */
void
swap_init (void) 
{
	swap_device = block_get_role (BLOCK_SWAP);
	if(swap_device){
//		printf("Device created . Initializing bitmap");
		swap_bitmap = bitmap_create (block_size(swap_device));
		if(swap_bitmap){
			//printf("bitmap created");
			bitmap_set_all(swap_bitmap,0);
		}
	}else{
	//	printf("Swap initialization failed \n");
	}
	lock_init(&swap_lock);
}

/* Swaps in page P, which must have a locked frame
   (and be swapped out). */
void
swap_in (struct page *p) 
{
	lock_acquire(&swap_lock);
	if(!swap_device){
		lock_release(&swap_lock);
		PANIC("SWAP Device not available");
	}

	// Start index stored in page info
	size_t start = p->swapStart;
	// Address to which the frame is to be loaded
	void *readAddress = p->frame->base;
	size_t i;
	// Reading page sectors
	for(i=0;i<PAGE_SECTORS;i++){
		block_read(swap_device,start+i,readAddress+i*BLOCK_SECTOR_SIZE);
		// Flip the bitmap values
		bitmap_flip(swap_bitmap,start+i);
	}
	//Notified loaded info
	p->loaded=true;
//	p->swapped=false;

	lock_release(&swap_lock);
}

/* Swaps out page P, which must have a locked frame. */
bool
swap_out (struct page *p) 
{

	lock_acquire(&swap_lock);
	if(!swap_device){
		lock_release(&swap_lock);
		PANIC("SWAP Device not available");
	}
	
	//Find contiguous PAGE_SECTORS in block device
	size_t start = bitmap_scan_and_flip(swap_bitmap,0,PAGE_SECTORS,0);
	if(start == BITMAP_ERROR){
		//Bitmap is full Cannot write to bit map
		PANIC("Full SWAP");
	}

	void *readAddress = p->frame->base;
	// Address from which we should read
	size_t i;
//	printf("Swapped data at read Address %p to start index %d\n",readAddress,start);
	for(i=0;i<PAGE_SECTORS;i++){
		// Reading Page info and writing to blocks sectorwise
		block_write(swap_device,start+i,readAddress+i*BLOCK_SECTOR_SIZE);
	}
	//Successfully swapped and unloaded
	p->swapped=true;
	p->loaded=false;
	p->swapStart = start;
	lock_release(&swap_lock);
	return true;
}
