#include "vm/frame.h"
#include <stdio.h>
#include "vm/page.h"
#include "devices/timer.h"
#include "threads/init.h"
#include "threads/malloc.h"
#include "threads/palloc.h"
#include "threads/synch.h"
#include "threads/vaddr.h"
#include "threads/thread.h"

/* Initialize the frame manager. */

	void
frame_init (void) 
{
	list_init(&frameList);
	lock_init(&frametable_lock);
	// Initialize a list
	// Initialize list lock
	// Need to know what is means by locking a frame
}

struct frame * frame_alloc_swap(struct page *page){

//	printf("Frame alloc swap \n");
	struct frame *newFrame;
	uint8_t *kpage = palloc_get_page (PAL_USER | PAL_ZERO);
	if (kpage == NULL){
//		printf("FAILED to allocate a page \n");
		newFrame = lru_eviction();	
		frame_free(newFrame);
		kpage = palloc_get_page (PAL_USER | PAL_ZERO);
		if (kpage == NULL){
			PANIC("Unable to SWAP");
		}
		//return NULL;
		// Need to evict a page .. Sending null for temp purpose
	}else{
		newFrame = (struct frame *)malloc(sizeof(struct frame));
		lock_init(&(newFrame->frame_lock));	
		lock_acquire(&(newFrame->frame_lock));
	}
//	printf("Swapping in \n");
	swap_in(page);
//	printf("Swap in complete \n");
	if (!setFrame (page->addr, kpage, page->writable)) 
	{
		palloc_free_page (kpage);
//		printf("FAILED to allocate a page \n");

		//	printf("set frame unable to set the process address space\n");
		return NULL;
	}

	newFrame->base = kpage;
	newFrame->page = page;
	//	lock_init(newFrame->frame_lock);
	//	lock_acquire(newFrame->frame_lock);
	if(!lruLoc){
		lruLoc = &(newFrame->listElem);
//		printf("Setting lruLoc");
	}
	lock_acquire(&frametable_lock);
	list_push_back(&frameList,&(newFrame->listElem));
	lock_release(&frametable_lock);
//	printf("Frame List size::%d",list_size(&frameList));
	//printf("Frame allocation completed \n");
	return newFrame;

}

/* Tries to allocate and lock a frame for PAGE.
   Returns the frame if successful, false on failure. */
struct frame * frame_alloc_stack(struct page *page){
	struct frame *newFrame;
	//printf("Frame is allocated \n");
	uint8_t *kpage = palloc_get_page (PAL_USER | PAL_ZERO);
	if (kpage == NULL){
//		printf("FAILED to allocate a page \n");
		newFrame = lru_eviction();
		frame_free(newFrame);
		kpage = palloc_get_page (PAL_USER | PAL_ZERO);
		if (kpage == NULL){
			PANIC("Unable to SWAP");
		}
		//return NULL;
		// Need to evict a page .. Sending null for temp purpose
	}else{
		newFrame = (struct frame *)malloc(sizeof(struct frame));
		lock_init(&(newFrame->frame_lock));
		lock_acquire(&(newFrame->frame_lock));	
	}

	if (!setFrame (page->addr, kpage, page->writable)) 
	{
		palloc_free_page (kpage);
//		printf("FAILED to allocate a page \n");

		//	printf("set frame unable to set the process address space\n");
		return NULL;
	}

	newFrame->base = kpage;
	newFrame->page = page;
	//	lock_init(newFrame->frame_lock);
	//	lock_acquire(newFrame->frame_lock);
	if(!lruLoc){
		lruLoc = &(newFrame->listElem);
	}
	lock_acquire(&frametable_lock);
	list_push_back(&frameList,&(newFrame->listElem));
	lock_release(&frametable_lock);
//	printf("Frame List size::%d",list_size(&frameList));
	//printf("Frame allocation completed \n");
	return newFrame;

}

	struct frame* 
frame_alloc_and_lock (struct page *page) 
{

	struct frame *newFrame;
//	printf("Frame alloc and lock \n");
	uint8_t *kpage = palloc_get_page (PAL_USER | PAL_ZERO);
	if (kpage == NULL){
//		printf("Page allocation obtained \n");
		newFrame = lru_eviction();
//		printf("Frame eviction complete \n");
		//printf("Eviction Frame allocated %d \n",newFrame->page->addr);
		frame_free(newFrame);
		kpage = palloc_get_page (PAL_USER | PAL_ZERO);
		if (kpage == NULL){
			PANIC("Unable to SWAP");
		}
//		printf("Page allocation over \n");
		//return NULL;
		// Need to evict a page .. Sending null for temp purpose
	}else{
		newFrame = (struct frame *)malloc(sizeof(struct frame));
		lock_init(&(newFrame->frame_lock));
		lock_acquire(&(newFrame->frame_lock));
		//printf("New Frame allocated %d \n",page->addr);
	}

	/*  Load this page. */
	if (file_read_at (page->file, kpage, page->readBytes,page->ofs) != (int) page->readBytes)
	{
		palloc_free_page (kpage);
//		printf("File is corrupted .. Un expected behaviour");
		return NULL;
	}

	//	memset (kpage + page->readBytes, 0, (PGSIZE-page->readBytes));

	/* Add the page to the process's address space. */
	if (!setFrame (page->addr, kpage, page->writable)) 
	{
		palloc_free_page (kpage);
		//	printf("set frame unable to set the process address space\n");
		return NULL;
	}

	newFrame->base = kpage;
	newFrame->page = page;

	if(!lruLoc){
		lruLoc = &(newFrame->listElem);
	}
	lock_acquire(&frametable_lock);
	list_push_back(&frameList,&(newFrame->listElem));
	lock_release(&frametable_lock);
//	printf("Frame List size::%d",list_size(&frameList));

	//printf("Frame allocation completed \n");
	return newFrame;
}

/* Locks P's frame into memory, if it has one.
   Upon return, p->frame will not change until P is unlocked. */

	void
frame_lock (struct page *p) 
{
	lock_acquire(&(p->frame->frame_lock));
}


/* Releases frame F for use by another page.
   F must be locked for use by the current process.
   Any data in F is lost. */

	void
frame_free (struct frame *f)
{
	palloc_free_page(f->base);
	f->base=NULL;
	f->page->frame=NULL;
	f->page=NULL;
//	free(f);
}


/* Unlocks frame F, allowing it to be evicted.
   F must be locked for use by the current process. */

	void
frame_unlock (struct frame *f) 
{
	lock_release(&(f->frame_lock));
}

bool setFrame (void *upage, void *kpage, bool writable)
{
	struct thread *t = thread_current ();

	/* Verify that there's not already a page at that virtual
	   address, then map our page there. */
	return (pagedir_get_page (t->pagedir, upage) == NULL
			&& pagedir_set_page (t->pagedir, upage, kpage, writable));
}

struct frame *lru_eviction(){

	// start from lruListElem and form a rotation
	//
	lock_acquire(&frametable_lock);
	struct list_elem *itElem = lruLoc;
	struct frame *retFrame = NULL;
//	printf("Size::%d\n",list_size(&frameList));	
	while(retFrame != NULL){
		struct frame *frame = list_entry(itElem,struct frame,listElem);
		bool dirty = pagedir_is_dirty(frame->page->pagedir,frame->page->addr);
		bool accessed = pagedir_is_accessed(frame->page->pagedir,frame->page->addr);
		if(!accessed){
	//		printf("Found frame!!\n");
			if(dirty || frame->page->swapped){
	//			printf("Swapped out!!\n");
				swap_out(frame->page);
			}
			retFrame = frame;
			lruLoc = list_remove(&frame->listElem);
			//frame_free(frame);
//			printf("frame eviction address %p\n",frame->page->addr);
			break;

		}else{
		//	printf("Rotating \n");
			pagedir_set_accessed(frame->page->pagedir,frame->page->addr,false);
		}
		itElem = list_next(itElem);
		if (itElem == list_end(&frameList)){
//			printf("End of list\n");
//			printf("Rotation \n");
//			printf("End Size::%d\n",list_size(&frameList));
			itElem = list_begin(&frameList);
		}
	}
	//printf("Evicted a frame");
	lock_release(&frametable_lock);
	return retFrame;
}
