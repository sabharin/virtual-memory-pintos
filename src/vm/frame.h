#ifndef VM_FRAME_H
#define VM_FRAME_H

#include <stdbool.h>
#include "threads/synch.h"

/* A physical frame. */
struct frame 
{
    void *base;                 /* Kernel virtual base address. */
    struct page *page;          /* Mapped process page, if any. */
    bool secondChance;
    struct list_elem listElem;
    struct lock frame_lock;
    /* ...............          other struct members as necessary */
};

struct lock frametable_lock;
struct list frameList;
struct list_elem *lruLoc;
void frame_init (void);
struct frame *lru_eviction();

struct frame *frame_alloc_swap(struct page *);
struct frame *frame_alloc_stack(struct page *);
struct frame *frame_alloc_and_lock (struct page *);
void frame_lock (struct page *);

void frame_free (struct frame *);
void frame_unlock (struct frame *);
bool setFrame (void *, void *, bool );

#endif /* vm/frame.h */
