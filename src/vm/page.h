#ifndef VM_PAGE_H
#define VM_PAGE_H

#include <hash.h>
#include "devices/block.h"
#include "filesys/off_t.h"
#include "threads/synch.h"
#define STACK_MAX (8 * 1024 * 1024)

/* Virtual page. */
struct page 
  {
	struct frame *frame;        /* Page frame. */
	struct file* file;
	off_t ofs;
	size_t readBytes;
	void *addr;
	bool writable;
	struct hash_elem hashElem;
	bool loaded;
	bool swapped;
	uint32_t *pagedir;
	size_t swapStart;
	struct lock page_lock;
   
     /* ...............          other struct members as necessary */     

  };

void page_exit (struct hash *);
bool pagetable_create (struct hash *);
bool pagetable_destroy(struct hash *);
struct page *page_allocate (struct file *, off_t ,size_t ,void *, bool );
void page_deallocate (void *);
bool page_eviction();
struct page *page_for_addr (const void *);
bool do_page_in(struct page *);
bool page_in (void *fault_addr);
bool page_out (struct page *);
bool page_accessed_recently (struct page *);

bool page_lock (const void *, bool will_write);
void page_unlock (const void *);

hash_hash_func page_hash;
hash_less_func page_less;

#endif /* vm/page.h */
